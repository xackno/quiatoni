@extends('layouts.app')

@section('content')<br><br><br>




<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-secondary text-white">
                    <h3><i class="fas fa-user"></i> {{ __('registros') }}</h3>
                </div>

                <div class="card-body">
                    <div id="para_alert"></div>
                    
                    <form action="{{url('/registrar_persona')}}" method="post" id="form_persona">
                        @csrf
                        <div class="row">
                            <div class="col-12 col-lg-4 col-md-4">
                                <div class="row">
                                    <div class="col">
                                        <label><b>Nombre</b>  </label>
                                        <input type="text" name="nombres" class="form-control" placeholder="nombres / nombres" style="text-transform:uppercase;" pattern="[A-Z]" autofocus="" required="" title="Es necesario">
                                    </div>
                                    <div class="col">
                                        <label><b>Apellidos</b></label>
                                <input type="text" name="apellidos" class="form-control" placeholder="Apellidos completo" style="text-transform:uppercase;" required="" autofocus="" title="Es necesario">
                                    </div>
                                </div>
                                <label><b>Sexo</b></label>
                                <select class="form-control" name="sexo">
                                    <option selected="true" disabled="true">Selecciones una opción</option>
                                    <option value="masculino">Masculino</option>
                                    <option value="femenino">Femenino</option>
                                </select>

                                <label><b>Fecha de nacimiento</b></label>
                                <input type="date" name="fecha_nacimiento" class="form-control">
                            </div>
                            <div class="col-12 col-lg-4 col-md-4">
                                <label><b>Escolaridad</b> </label>
                                <select name="escolaridad" class="form-control">
                                    <option disabled="true" selected="">Seleccione una opción</option>
                                    <option value="ninguna">Ninguna</option>
                                    <option value="inicial">Educación inicial</option>
                                    <option value="preescolar">Preescolar</option>
                                    <option value="primaria">Primaria</option>
                                    <option value="secundaria">Secundaria</option>
                                    <option value="bachillerato">Bachillerato</option>
                                    <option value="licenciatura">Licenciatura</option>
                                    <option value="maestria">Maestría</option>
                                    <option value="doctorado">Doctorado</option>
                                </select>
                                <label><b>Núcleo rural registrado</b></label>
                                <select class="form-control" name="nucleo_rural_registrado">
                                    <option selected="true" disabled="true">Selecciones una opción</option>
                                    <option value="San Pedro">San pedro</option>
                                </select>

                                <label><b>Núcleo rural residencia</b></label>
                                <select class="form-control" name="nucleo_rural_residencia">
                                    <option selected="true" disabled="true">Selecciones una opción</option>
                                    <option value="San Pedro">San pedro</option>
                                </select>
                            </div>



                            <div class=" col-12 col-lg-4 col-md-4">
                                <label><b>Ocupación</b></label>
                                <input type="text" name="ocupacion" class="form-control" style="text-transform:uppercase;" required="" title="Es necesario">
                                <br>
                                <label><b>Estado civil</b></label>
                                <select name="estado_civil" class="form-control">
                                    <option selected="" disabled="">Selecciones una opción</option>
                                    <option value="soltero/a">Soltero/a</option>
                                    <option value="casado/a">Casado/a</option>
                                    <option value="union libre">Unión libre o unión de hecho</option>
                                    <option value="separado">Separado/a</option>
                                    <option value="divorciado/a">Divorciado/a</option>
                                    <option value="viudo/a">Viudo/a</option>
                                </select>
                            </div>
                        </div>
                        <br><br>
                        <button class="btn btn-primary float-right" type="submit" id="btn_submit">Registrar <i class="fas fa-user-check"></i></button>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script type="text/javascript">


$("#btn_submit").click(function(e){
    $("#form_persona").validate();
    e.preventDefault();
    $.ajax({
        url:"{{url('/registrar_persona')}}",
        type:"post",
        dataType:"json",
        data:$("#form_persona").serialize(),
        success:function(e){
            // alert(e);
            $("#para_alert").html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+' Se <strong>REGISTRÓ</strong> correctamente esta persona en el sistema.'
                +'</div>' );
            $("#form_persona")[0].reset();
            setInterval(function(){
                $("#para_alert").html("");
            },8000);
        },
        error:function(){
            $("#para_alert").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+' Se <strong>ERROR</strong> No fue posible registrar esta persona, verifica la información proporcionada.'
                +'</div>' );
            setInterval(function(){
                $("#para_alert").html("");
            },4000);
        }
    });

});

    </script>
@endsection