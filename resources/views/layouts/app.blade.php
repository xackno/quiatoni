<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>QUIATONI <?php echo date("Y") ?></title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <link rel="icon" href="{{asset('/img/logo.png')}}" type="image/x-icon"/>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- CSS Files -->
  <link rel="stylesheet" href="{{asset('/css/atlantis.css')}}">
  <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">
  
  <link rel="stylesheet" type="text/css" href="{{asset('/css/fonts.min.css')}}">
  <link rel="stylesheet" href=" {{asset('/css/demo.css')}}">
</head>








<body>
  <div class="wrapper" id="wrapper">

    <div class="main-header">
      <!-- Logo Header -->
      <div class="logo-header" data-background-color="blue3" style="background: #0E6655">
        <a href="" class="logo" >
          <span class="text-white text-center"><b>San Pedro Quiatoni</b></span>
        </a>

        <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation" >
          <span class="navbar-toggler-icon">
            <i class="icon-menu"></i>
          </span>
        </button>
        <button class="topbar-toggler more" >
          <i class="icon-options-vertical"></i>
        </button>
        <div class="nav-toggle">
          <button class="btn btn-toggle toggle-sidebar">
          <i class="icon-menu"></i>
        </button>
        </div>
      </div>
      <!-- End Logo Header -->

      <!-- Navbar Header -->

      <nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue3" style="background: #0E6655">


        <div class="container-fluid">

          <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">

            <li class="nav-item dropdown hidden-caret">
              <a class="nav-link dropdown-toggle" href="#" id="notifDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-bell"></i>
                <span class="notification">1</span>
              </a>
              <ul class="dropdown-menu notif-box animated fadeIn" aria-labelledby="notifDropdown">
                <li>
                  <div class="dropdown-title">You have 1 new notification</div>
                </li>
                <li>
                  <div class="notif-scroll scrollbar-outer">
                    <div class="notif-center">
                      <a href="#">
                        <div class="notif-icon notif-primary"> <i class="fa fa-user-plus"></i> </div>
                        <div class="notif-content">
                          <span class="block">
                            New user registered
                          </span>
                          <span class="time">5 minutes ago</span>
                        </div>
                      </a>
                    </div>
                  </div>
                </li>
                <li>
                  <a class="see-all" href="javascript:void(0);">See all notifications<i class="fa fa-angle-right"></i> </a>
                </li>
              </ul>
            </li>
            <li class="nav-item dropdown hidden-caret">
              <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                <i class="fas fa-layer-group"></i>
              </a>
              <div class="dropdown-menu quick-actions quick-actions-info animated fadeIn">
                <div class="quick-actions-header">
                  <span class="title mb-1">Quick Actions</span>
                  <span class="subtitle op-8">Shortcuts</span>
                </div>
                <div class="quick-actions-scroll scrollbar-outer">
                  <div class="quick-actions-items">
                    <div class="row m-0">
                      <a class="col-6 col-md-4 p-0" href="#">
                        <div class="quick-actions-item">
                          <i class="flaticon-file-1"></i>
                          <span class="text">Generated Report</span>
                        </div>
                      </a>
                      <a class="col-6 col-md-4 p-0" href="#">
                        <div class="quick-actions-item">
                          <i class="flaticon-database"></i>
                          <span class="text">Create New Database</span>
                        </div>
                      </a>

                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li class="nav-item dropdown hidden-caret">
              <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
                <div class="avatar-sm" >
                  <img src="{{asset('/img/logo.png')}}"  class="avatar-img rounded-circle" >
                </div>
              </a>
              <ul class="dropdown-menu dropdown-user animated fadeIn">
                <div class="dropdown-user-scroll scrollbar-outer">
                  <li>
                    <div class="user-box">
                      <div class="avatar-lg">
                        <img src="{{asset('/img/logo.png')}}" alt="image profile" class="avatar-img rounded">
                      </div>
                      <div class="u-text">
                        <h4>@if(isset(Auth::user()->nombre)){{Auth::user()->nombre }}@endif</h4>
                        <p class="text-muted"> @if(isset(Auth::user()->email)){{Auth::user()->email }}@endif</p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="dropdown-divider"></div>
                    @if(isset(Auth::user()->tipo_usuario) =='administrador')
                      <a class="dropdown-item" href="{{url('/registrar')}}"><i class="fas fa-plus"></i> Registrar usuario</a>
                      <a class="dropdown-item" href="#"><i class="fas fa-user text-primary"></i> My perfil</a>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                      <i class="fas fa-power-off text-success"></i>
                      {{ __('Salir') }}
                  </a>
                    @else
                    <!-- <a  class="dropdown-item" href="{{route('home')}}"> <i class="fas fa-home"></i> Inicio</a> -->
                    <a  class="dropdown-item" href="{{url('/login')}}"><i class="fas fa-user"></i> Entrar</a>
                    @endif
                    
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                      @csrf
                  </form>
                  </li>
                </div>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
      <!-- End Navbar -->

    </div>
  










    <!-- Sidebar -->
    <div class="sidebar sidebar-style-" style="background: #E8F6F3">
      <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
          <div class="user">
            <div class="avatar-sm float-left mr-2">
              <img src="{{asset('/img/logo.png')}}" alt="..." class="avatar-img rounded-circle">
            </div>
            <div class="info">
              <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                <span>
                  @if(isset(Auth::user()->nombre)){{Auth::user()->nombre }}@endif
                  <span class="user-level">@if(isset(Auth::user()->tipo_usuario)){{Auth::user()->tipo_usuario }}@endif</span>
                  <span class="caret"></span>
                </span>
              </a>
              <div class="clearfix"></div>

              <div class="collapse in" id="collapseExample">
                <ul class="nav">
                  <li>
                    <a href="#settings">
                      <span class="link-collapse">Settings</span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <ul class="nav nav-info">
            <li class="nav-section">
              <span class="sidebar-mini-icon">
                <i class="fa fa-ellipsis-h"></i>
              </span>
              <h4 class="text-section">Menú</h4>
            </li>
            <li class="nav-item active">
              <a href="{{route('home')}}">
                <i class="fas fa-home text-dark"></i>
                <p>Inicio</p>
              </a>
            </li>
            <li class="nav-item">
              <a data-toggle="collapse" href="#sidebarLayouts">
                <i class="fas fa-clipboard-list text-success"></i>
                <p>Registros</p>
                <span class="caret"></span>
              </a>
              <div class="collapse" id="sidebarLayouts">
                <ul class="nav nav-collapse nav-primary">
                  <li class="nav-item ">
                    <a href="{{url('/persona')}}" >
                      <i class="fas fa-plus text-success"></i>
                      <p>Persona</p>
                      <i class="fas fa-user"></i>
                    </a>
                  </li>
                  <li class="nav-item ">
                    <a href="{{url('/familias')}}" >
                      <i class="fas fa-users text-success"></i>
                      <p>Familias</p>
                    </a>
                  </li>
                  <li class="nav-item ">
                    <a href="{{url('/servicios')}}" >
                      <i class="fas fa-school text-success"></i>
                      <p>Servicios</p>
                    </a>
                  </li>
                </ul>

              </div>
            </li>
            
            <li class="nav-item ">
              <a href="{{url('/registros')}}" >
                <i class="fas fa-table text-primary"></i>
                <p>Población</p>
              </a>
            </li>
            
            
            <!-- ######################copyright -->
            <li class="nav-item " style="bottom: 2px;position: absolute;"><a href=""> &copy QUIATONI, {{date("Y")}}.</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- End Sidebar -->
    <style type="text/css">
      .sidebar_minimize .sidebar {
        width: 0px;
        transition: all .3s;
        display: none;
      }
        .sidebar_minimize .main-panel {
        width: calc(100% - 0px);
        transition: all .3s; }

      .sidebar_minimize .logo-header{
        /*display: none;*/
      }
    </style>

    <div class="main-panel">
      @yield('content')
    </div>
  </div>
  <script src="{{asset('js/core/jquery.3.2.1.min.js')}}"></script>
  <script src="{{asset('/js/core/popper.min.js')}}"></script>
  <script src="{{asset('/js/core/bootstrap.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
  

  <script src="{{asset('/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
  <script src="{{asset('/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js')}}"></script>
  <script src="{{asset('/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>
  <script src="{{asset('/js/plugin/chart.js/chart.min.js')}}"></script>
  <script src="{{asset('/js/plugin/jquery.sparkline/jquery.sparkline.min.js')}}"></script>
  <script src="{{asset('/js/plugin/chart-circle/circles.min.js')}}"></script>
  <script src="{{asset('/js/plugin/datatables/datatables.min.js')}}"></script>
  <script src="{{asset('/js/plugin/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
  <script src="{{asset('/js/plugin/jqvmap/jquery.vmap.min.js')}}"></script>
  <script src="{{asset('/js/plugin/jqvmap/maps/jquery.vmap.world.js')}}"></script>
  <script src="{{asset('/js/plugin/sweetalert/sweetalert.min.js')}}"></script>
  <script src="{{asset('/js/atlantis.min.js')}}"></script>
  <script src="{{asset('/js/setting-demo.js')}}"></script>
  <!-- <script src=".{{asset('/js/demo.js')}}"></script> -->
  @yield('script')
  <script>
    if ($(window).width() <= 360) {
     }if($(window).width() >= 361){
       $("#wrapper").addClass("sidebar_minimize");
     }
    $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

    $("#icon-home").hover(function(){
      $(".hovers").fadeIn('slow');
    });
    $("#icon-home").mouseleave(function(){
      $(".hovers").fadeOut("slow");

    });



//#################OBTENER FECHA Y HORA#########################
  //funcion obteniendo fecha
  function fecha(){
    var fecha = new Date();
    var dia="";var mes="";
    var meses=['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
    if (fecha.getDate()<10) {
      dia="0"+fecha.getDate();
    }else{
      dia=fecha.getDate();
    }

    var fechahoy=(dia+"/"+meses[fecha.getMonth()]+"/"+fecha.getFullYear());
    return fechahoy;
  }

  function hora(){
    var fecha = new Date();
    var h="";var m=""; var s="";
    if (fecha.getHours()<10) {h="0"+fecha.getHours();}else{h=fecha.getHours();}
    if (fecha.getMinutes()<10) {m="0"+fecha.getMinutes();}else{m=fecha.getMinutes();}
    if (fecha.getSeconds()<10) {s="0"+fecha.getSeconds();}else{s=fecha.getSeconds();}
    var horahoy=h+":"+m+":"+s;
    return horahoy;
  }
  $("#fecha").text(fecha());
  setInterval(function(){ $("#hora_footer").text(hora());}, 1000);
//################################################################

  </script>
</body>
</html>
