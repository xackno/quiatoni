@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="panel-header bg-primary-gradient">
          <div class="page-inner py-5">
            <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
              <div>
                <div class="row">
                  <div class="col">
                    <img src="{{asset('img/logo.png')}}" style="width: 120px">
                  </div>
                  <div class="col">
                    <h1 class="text-white pb-2 fw-bold">San pedro Quiatoni</h1>
                  <h5 class="text-white op-7 mb-2">Población Total SPQ</h5>
                  </div>
                </div>


              </div>
              <div class="ml-md-auto py-2 py-md-0">
                <!-- <a href="http://stehs.com" class="btn btn-white btn-border btn-round mr-2" target="_blank">Página Oficial</a>
                <p class="text-white">Desarrollo de sistemas web a medida.</p> -->
                <!-- <a href="#" class="btn btn-secondary btn-round">Add Customer</a> -->
              </div>
            </div>
          </div>
    </div>



  </div>

@endsection
