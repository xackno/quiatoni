<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

// Auth::routes();
Route::get('login', "Auth\LoginController@showLoginForm")->name("login");
Route::post("/logout", "Auth\LoginController@logout")->name("logout");
Route::post("/login", "Auth\LoginController@login");

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::middleware(['auth', 'verified'])->group(function () {
	Route::get("/registrar", "generalController@registrar"); //para mostrar la vista de regsitrar
    Route::post("/register", "Auth\RegisterController@register")->name("register"); //ejecutar form


		Route::get("/persona", "registrosController@index");
		Route::get("/familias", "registrosController@familias");
		Route::get("/servicios", "registrosController@servicios");

		Route::post("/registrar_persona","registrosController@registrar_persona");


});