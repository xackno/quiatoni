<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class personas extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'nombres',
        'apellidos',
        'sexo',
        'fecha_nacimiento',
        'escolaridad',
        'nucleo_rural_registrado',
        'nucleo_rural_residencia',
        'ocupacion',
        'estado_civil',
        'created_at',
        'updated_at',
    ];
}
